#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int, char**) {
	// open the video file for reading
	VideoCapture cap("F:/Google Cloud - Shermannatrix/Social Media Posts/LinkedIn/2021-09-18_08-18-24.mp4");

	// if not successful, exit program
	if (cap.isOpened() == false) {
		cout << "Cannot open the video file" << endl;
		cin.get();
		return -1;
	}

	// get the frame rates of the video
	double fps = cap.get(CAP_PROP_FPS);
	cout << "Frames per second: " << fps << endl;

	String window_name = "My First Video";

	namedWindow(window_name, WINDOW_NORMAL);
	resizeWindow(window_name, 1280, 720);

	while (true) {
		Mat frame;
		bool bSuccess = cap.read(frame);

		// Breaking the while loop at the end of the video
		if (!bSuccess) {
			cout << "Found the end of the video file" << endl;
			break;
		}

		// show the frame in the created window
		imshow(window_name, frame);

		if (waitKey(17) == 27) {
			cout << "Esc key is pressed by user. Stopping the video" << endl;
			break;
		}
	}

	return 0;
}
