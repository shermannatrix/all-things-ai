#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int, char**) {
	// Open the default video camera
	VideoCapture cap(1);
	cap.set(CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(CAP_PROP_FRAME_HEIGHT, 720);

	// if not successful, exit program
	if (!cap.isOpened()) {
		cout << "Cannot open the video camera" << endl;
		cin.get();
		return -1;
	}

	int frame_width = static_cast<int>(cap.get(CAP_PROP_FRAME_WIDTH));
	int frame_height = static_cast<int>(cap.get(CAP_PROP_FRAME_HEIGHT));

	Size frame_size(frame_width, frame_height);
	int frames_per_second = 10;

	// Create and initialize the VideoWriter object
	VideoWriter oVideoWriter("myvideo.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'),
		frames_per_second, frame_size, true);

	// If the VideoWriter object is not initialized successfully, exit the program
	if (!oVideoWriter.isOpened()) {
		cout << "Cannot save the video to a file" << endl;
		cin.get();
		return -1;
	}

	string window_name = "My Camera Feed";
	namedWindow(window_name);
	resizeWindow(window_name, 1280, 720);

	while (true) {
		Mat frame;
		bool isSuccess = cap.read(frame);	// read a new frame from the video camera

		// Breaking the while loop if frames cannot be read from the camera
		if (!isSuccess) {
			cout << "Video camera is disconnected" << endl;
			cin.get();
			break;
		}

		/*
		Make changes to the frame as necessary
		e.g.
			1. Change brightness/contrast of the image
			2. Smooth/Blue image
			3. Crop the image
			4. Rotate the image
			5. Draw shapes on the image
		*/

		// write the video frame to the file
		oVideoWriter.write(frame);

		// show the frame in the created window
		imshow(window_name, frame);

		// Wait for the 10ms until any key is pressed.
		// If the 'Esc' key is pressed, break the while loop
		// If any other key is pressed, continue the loop
		// If any key is not pressed within 10ms, continue the loop
		if (waitKey(10) == 27) {
			cout << "Esc key is pressed by the user. Stopping the video << endl";
			break;
		}
	}

	// Flush and close the video file
	oVideoWriter.release();

	return 0;
}
