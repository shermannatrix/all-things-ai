#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int, char**) {
	// Read the image file
	Mat image = imread("F:/Google Cloud - Shermannatrix/Social Media Posts/Gaming/FH4/gamer-handle.jpg");
	if (image.empty()) {
		cout << "Could not open or find the image" << endl;
		cin.get();
		return -1;
	}

	/*
	Make changes to the image as necessary
	e.g.
		1. Change brightness/contrast of the image
		2. Smooth/Blur image
		3. Crop the image
		4. Rotate the image
		5. Draw shapes on the image
	*/

	bool isSuccess = imwrite("saved_image.jpg", image);	// write the image to a file as a JPEG

	if (!isSuccess) {
		cout << "Failed to save the image" << endl;
		cin.get();
		return -1;
	}

	cout << "Image is successfully saved to a file" << endl;

	String windowName = "The Saved Image";
	namedWindow(windowName);	// Create a window
	imshow(windowName, image);	// Display our image inside the created window.

	waitKey(0);		// Wait for any keystroke in the window

	destroyWindow(windowName);

	return 0;
}
