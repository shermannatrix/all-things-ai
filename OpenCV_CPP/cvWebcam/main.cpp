#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int, char**) {
	// Open the default video camera
	VideoCapture cap(1);

	cap.set(CAP_PROP_FOURCC, VideoWriter::fourcc('M', 'J', 'P', 'G'));
    cap.set(CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(CAP_PROP_FRAME_HEIGHT, 720);

	// if not successful, exit program
	if (cap.isOpened() ==  false) {
		cout << "Cannot open the webcam" << endl;
		cin.get();
		return -1;
	}

	double dWidth = cap.get(CAP_PROP_FRAME_WIDTH);
	double dHeight = cap.get(CAP_PROP_FRAME_HEIGHT);

	cout << "Resolution of the video : " << dWidth << " x " << dHeight << endl;

	string window_name = "My Webcam Feed";
	namedWindow(window_name);
	resizeWindow(window_name, 1280, 720);

	while (true) {
		Mat frame;
		bool bSuccess = cap.read(frame);

		// Breaking the while loop is the frames cannot be captured
		if (!bSuccess) {
			cout << "Webcam is disconnected" << endl;
			cin.get();
			break;
		}

		// show the frame in the created window
		imshow(window_name, frame);

		// wait for xx seconds until any key is pressed.
		// If the 'Esc' key is pressed, break the while loop.
		// If any other key is pressed, continue the loop
		// If any key is not pressed within xx seconds, continue the loop
		if (waitKey(360) == 27) {
			cout << "Esc key is pressed by user. Stopping the video" << endl;
			break;
		}
	}

	return 0;
}
